#!/bin/sh

set -e

FUNC='WF'

cd ../fqidrive-server
./gradlew json jsonED
cd -

if test "$1" = "early-detection"
then
FUNC='ED'
    ./node_modules/.bin/mocha --opts test/mocha-ed.opts 2>&1 | tee TestOutputWithColors$FUNC.txt
elif test "$1" = "common"
then
FUNC='COMMON'
    ./node_modules/.bin/mocha --opts test/mocha-common.opts 2>&1 | tee TestOutputWithColors$FUNC.txt
elif test "$1" = "drive"
then
FUNC='DRIVE'
    ./node_modules/.bin/mocha --opts test/mocha-drive.opts 2>&1 | tee TestOutputWithColors$FUNC.txt
elif test "$1" = "search"
then
FUNC='SEARCH'
    ./node_modules/.bin/mocha --opts test/mocha-search.opts 2>&1 | tee TestOutputWithColors$FUNC.txt
else
    ./node_modules/.bin/mocha 2>&1 | tee TestOutputWithColors$FUNC.txt
    #Windows PC setup
    #./node_modules/.bin/mocha 2>&1
fi

if grep 'Warning' TestOutputWithColors$FUNC.txt
then
  LIGHTRED='\033[1;31m'
  NOCOLOR='\033[0m' # No Color
  echo "\n${LIGHTRED}** Please check the above warnings before committing. **${NOCOLOR}\n";
fi
