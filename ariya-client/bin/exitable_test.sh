#!/bin/sh

set -e

cd ../fqidrive-server
./gradlew json
cd -

./node_modules/.bin/mocha --no-timeouts

./bin/printClient.sh
