import React from 'react'
import AppHeader from './AppHeader'
import Routes from './Routes'
import FlexColumnDiv from './Containers/FlexColumnDiv'

export default class AppContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      language: 'english'
    }
  }

  render() {
    return (
      <FlexColumnDiv>
        <AppHeader
          language={this.state.language}
          history={this.props.history}
          location={this.props.location}
        />
        <FlexColumnDiv className='page-container'>
          <Routes/>
        </FlexColumnDiv>
      </FlexColumnDiv>
    )
  }
}