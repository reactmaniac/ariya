import PropTypes from 'prop-types'
import React from 'react'

export default class PageContainerPrimaryHeader extends React.Component {
  render() {
    return (
      <div className='top-header'>
        <div className='header-elements'>
          <div className='header-elements-left'>
            <h2 className='h2'>{this.props.title}</h2>
            <div className='subtitle'>{this.props.subtitle}</div>
          </div>
          <div className='header-elements-right'>
            {this.props.rightElement}
          </div>
        </div>
      </div>
    )
  }
}

PageContainerPrimaryHeader.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  rightElement: PropTypes.element
}

