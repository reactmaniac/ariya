import PropTypes from 'prop-types'
import React from 'react'
import openCaret from '../../images/open_caret.svg'
import closedCaret from '../../images/closed_caret.svg'

export default class AccordionContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: true
    }

    this.caretWasClicked = this.caretWasClicked.bind(this)
  }

  caretWasClicked() {
    this.setState({isOpen: !this.state.isOpen})
  }

  displayCaret() {
    if (this.state.isOpen) {
      return (<img className='open-caret' src={openCaret} onClick={this.caretWasClicked}/>)
    } else {
      return (<img className='closed-caret' src={closedCaret} onClick={this.caretWasClicked}/>)
    }
  }

  displayContent() {
    if (this.state.isOpen) {
      return (
        <div>
          {this.props.children}
        </div>
      )
    }
  }

  render() {
    return (
      <div className={`${this.props.className} document-field-group wrap-component`}>
        <div className='section-content accordion'>
          {this.displayCaret()}
          <h2 id={this.props.className} className='h2 title'>{this.props.title}</h2>
        </div>
        {this.displayContent()}
      </div>
    )
  }
}

AccordionContainer.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string
}