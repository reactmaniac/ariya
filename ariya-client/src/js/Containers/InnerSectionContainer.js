import PropTypes from 'prop-types'
import React from 'react'

export default class InnerSectionContainer extends React.Component {
  render() {
    return (
      <div className={this.props.className}>
        <h2 className='h2 title section-border bbs'>
          {this.props.title}
        </h2>
        <div className='section-content'>
          {this.props.children}
        </div>
      </div>
    )
  }
}

InnerSectionContainer.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string
}
