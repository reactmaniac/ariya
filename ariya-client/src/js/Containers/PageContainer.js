import React from 'react'

export default class PageContainer extends React.Component {
  render() {
    return (
      <div className='page-container' id={this.props.id}>
        {this.props.children}
      </div>
    )
  }
}
