import React from 'react'
import PropTypes from 'prop-types'

export default class FlexColumnDiv extends React.Component {
  render() {
    let className = 'flex-column-div'
    if (this.props.className) {
      className = `flex-column-div ${this.props.className}`
    }

    return (
      <div className={className} id={this.props.id}>
        {this.props.children}
      </div>
    )
  }
}

FlexColumnDiv.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any
}