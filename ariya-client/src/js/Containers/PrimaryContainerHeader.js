import PropTypes from 'prop-types'
import React from 'react'

export default class PrimaryContainerHeader extends React.Component {
  render() {
    return (
      <div className='page-title title'>
        <h2 className='h2'>
          {this.props.subjectPrefix}
          {this.props.subject ? ': ' : ''}
          {this.props.subject}
        </h2>
        <div className='sub-title'>
          {this.props.referenceNumber}
        </div>
      </div>
    )
  }
}

PrimaryContainerHeader.propTypes = {
  subjectPrefix: PropTypes.string.isRequired,
  subject: PropTypes.string,
  referenceNumber: PropTypes.string
}
