import PropTypes from 'prop-types'
import React from 'react'

export default class PageContainerFooter extends React.Component {
  render() {
    return (
      <div className='page-footer'>
        <div className='top-footer'>
          <div className='footer-elements'>
            <div className='footer-elements-right'>
              {this.props.rightElement}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

PageContainerFooter.propTypes = {
  language: PropTypes.string,
  rightElement: PropTypes.element.isRequired
}
