import PropTypes from 'prop-types'
import React from 'react'
import {translate} from '../localization/localizer'


export default class PageContainerHeaderWithStatus extends React.Component {
  render() {


    return (
      <div className='page-header'>
        <div className='top-header'>
          <div className='header-elements'>
            <div className='header-elements-left'>
              <h2 className='h2'>{this.props.title}</h2>
              <div className='subtitle ref-number'>{this.props.subtitle}</div>
            </div>
            <div className='header-elements-right'>
              {this.props.rightElement}
            </div>
          </div>
        </div>
        <div className='status'>

        </div>
      </div>
    )
  }
}

PageContainerHeaderWithStatus.propTypes = {
  language: PropTypes.string,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  rightElement: PropTypes.element,
  workflow: PropTypes.shape({
    status: PropTypes.string
  }).isRequired
}
