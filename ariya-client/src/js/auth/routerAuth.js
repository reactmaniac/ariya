import {isLoggedIn} from './loginUser'

export const requireAuth = (nextState, replace) => {
  if (!isLoggedIn()) {
    replace({
      pathname: '/login',
      state: { redirectPathname: nextState.location.pathname}
    })
  }
}
