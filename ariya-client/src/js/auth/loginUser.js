import localStorage from 'localStorage'

export const LOGGED_IN_USER_KEY = 'loggedInUser'
export const X_AUTH_TOKEN_KEY = 'xAuthToken'

export const setLoggedInUser = (loggedInUser) => {
  const user = {
    id: loggedInUser.id,
    username: loggedInUser.username,
    fullname: loggedInUser.fullname
  }
  localStorage.setItem(LOGGED_IN_USER_KEY, JSON.stringify(user))
}

export const setToken = (xAuthToken) => {
  localStorage.setItem(X_AUTH_TOKEN_KEY, xAuthToken)
}

export const getToken = () => {
  return localStorage.getItem(X_AUTH_TOKEN_KEY)
}

export const removeLoggedInUser = () => {
  localStorage.removeItem(LOGGED_IN_USER_KEY)
  localStorage.removeItem(X_AUTH_TOKEN_KEY)
}

export const getLoggedInUser = () => {
  let loggedInUser = { id: -1, username: '', fullname: '' }

  const savedUser = localStorage.getItem(LOGGED_IN_USER_KEY)

  if (savedUser) {
    loggedInUser = JSON.parse(savedUser)
  }

  return loggedInUser
}

export const isLoggedIn = () => {
  const storageUser = localStorage.getItem(LOGGED_IN_USER_KEY)
  const storageToken = localStorage.getItem(X_AUTH_TOKEN_KEY)

  if (storageUser && storageToken) {
    return true
  }

  return false
}
