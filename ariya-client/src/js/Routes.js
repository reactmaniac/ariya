import React from 'react'
import {
  Route,
  Switch
} from 'react-router-dom'
import MyGroupsPage from './Workspace/MyGroupsPage'
import AssignedToMePage from './Workspace/AssignedToMePage'
import SearchPage from './Search/SearchPage'
import CreatedByMePage from './Workspace/CreatedByMePage'
import FlexColumnDiv from './Containers/FlexColumnDiv'

export default class Routes extends React.Component {
  render() {
    return (
      <FlexColumnDiv>
        <Route exact path='/' component={MyGroupsPage}/>
        <Route exact path='/assignedtome' component={AssignedToMePage}/>
        <Route exact path='/createdbyme' component={CreatedByMePage}/>
        <Route exact path='/search' component={SearchPage}/>
      </FlexColumnDiv>
    )
  }
}
