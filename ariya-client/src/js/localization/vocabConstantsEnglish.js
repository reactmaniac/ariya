export const english = {
  BOOLEAN_VALUE_TRUE: 'yes',
  BOOLEAN_VALUE_FALSE: 'no',

  ALL_ATTACHMENTS_TITLE: 'all attachments',
  ATTACHMENTS_VIEW_ALL: 'view all',
  REJECT_DOCUMENT_TITLE: 'reject document',
  REJECT_DOCUMENT_DESCRIPTION: 'Are you sure you want to reject this document? This action cannot be undone.',
  REJECT_DOCUMENT_REASON: 'rejection reason',

  ERROR_MESSAGE_INVALID_DATE: 'please enter a valid date',
  ERROR_MESSAGE_LOGIN: 'please enter the correct username and password.',
  ERROR_MESSAGE_PROJECT: 'this project doesn\'t exist.',
  ERROR_MESSAGE_TECHREPORT: 'this technical report doesn\'t exist.',

  SAVE_BUTTON_TEXT: 'save',
  EDIT_BUTTON_TEXT: 'edit',
  UPLOAD_BUTTON_TEXT: 'upload',
  CONNECT_BUTTON_TEXT: 'connect',
  ADD_BUTTON_TEXT: 'add',
  REJECT_BUTTON_TEXT: 'reject',
  CANCEL_BUTTON_TEXT: 'cancel',

  DETAILS_LINK_TEXT: 'details',

  UPLOADING_FILE_MESSAGE: 'uploading file.',

  APPROVER_STATUS_APPROVED: 'approved',

  WORKFLOW_STATUS_DRAFT: 'draft',
  WORKFLOW_STATUS_AWAITING_APPROVAL: 'awaiting approval',
  WORKFLOW_STATUS_ERROR: 'error',
  WORKFLOW_STATUS_REJECTED: 'rejected',
  WORKFLOW_STATUS_AWAITING_REPLY: 'awaiting reply',
  WORKFLOW_STATUS_REPLY_SENT: 'reply sent',
  WORKFLOW_STATUS_PUBLISHED: 'published',

  WORKFLOW_ACTION_SEND: 'send',
  WORKFLOW_ACTION_APPROVE: 'approve',
  WORKFLOW_ACTION_REJECT: 'reject',
  WORKFLOW_ACTION_REPLY: 'reply',
  WORKFLOW_ACTION_PUBLISH: 'publish',

  LOGIN_USERNAME_LABEL: 'username:',
  LOGIN_PASSWORD_LABEL: 'password:',
  SIGNIN_BUTTON_TEXT: 'sign in'

}
