import React from 'react'
import {
  getLoggedInUser,
  removeLoggedInUser
} from './auth/loginUser'
import {translate} from './localization/localizer'
import {
  HEADER_HOME_LINK,
  HEADER_LOGOUT_LINK,
  HEADER_SEARCH_LINK
} from './localization/vocabConstants'
import {LinkWrapper} from './wrappers/reactRouterWrappers'
import {currentUrl} from './wrappers/windowWrapper'
import {logout} from './fetch/httpFetcher'
import {getProject} from './fetch/resourceFetcher'

export default class AppHeader extends React.Component {
  constructor(props) {
    super(props)

    this.state = {}

    this.logoutButtonWasClicked = this.logoutButtonWasClicked.bind(this)
  }

  componentWillMount() {
    this.getProjectAndSetToState(this.props.location.pathname)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.location.pathname !== nextProps.location.pathname) {
      this.getProjectAndSetToState(nextProps.location.pathname)
    }
  }

  getProjectAndSetToState(pathname) {
    const pathComponents = pathname.split('/')
    const isCurrentlyInProjectDocument = pathComponents[1] === 'projects' && pathComponents[3] === 'q4cars'

    if (isCurrentlyInProjectDocument) {
      const projectId = pathComponents[2]
      getProject(projectId)
        .then((returnedProject) => {
          this.setState({project: returnedProject})
        })
    } else {
      this.setState({project: undefined})
    }
  }

  logoutButtonWasClicked() {
    removeLoggedInUser()
    logout()
    this.props.history.replace('/login')
  }

  createHomeNavigationItem(destinationPath, content, className) {
    const activeHomePaths = ['/', '/assignedtome', '/createdbyme']

    if (activeHomePaths.includes(this.props.location.pathname)) {
      return <div className={`nav-item ${className} active`}>{content}</div>
    }

    return <LinkWrapper className={`nav-item ${className}`} to={destinationPath}>{content}</LinkWrapper>
  }

  createNavigationItem(destinationPath, content, className) {
    if (destinationPath === this.props.location.pathname) {
      return <div className={`nav-item ${className} active`}>{content}</div>
    }

    return <LinkWrapper className={`nav-item ${className}`} to={destinationPath}>{content}</LinkWrapper>
  }

  render() {
    const language = this.props.language
    let appIconImage = <img className='header-icon' src={require(`../images/headerIcon.png`)}/>
    if (currentUrl().includes('localhost')) {
      appIconImage = <img className='header-icon' src={require(`../images/headerIcon_dev.png`)}/>
    }

    const searchLinkContent = (
      <div className='search-icon-link'>
        <img className='search-icon' src={require(`../images/search_icon.svg`)}/>
        <span>{translate(HEADER_SEARCH_LINK, this.props.language)}</span>
      </div>
    )

    const showProjectBreadcrumb = () => {
      if (this.state.project) {
        return (
          <div className='breadcrumb-component'>
            <div className='breadcrumb-divider'>/</div>
            <LinkWrapper className='project' to={`/projects/${this.state.project.id}`}>
              {this.state.project.referenceNumber + ': ' + this.state.project.subject}
            </LinkWrapper>
          </div>
        )
      }
    }

    return (
      <header className='header-container'>
        <div className='header'>
          <div className='navigation'>
            {appIconImage}
            <div className='breadcrumbs'>
              {this.createHomeNavigationItem('/', translate(HEADER_HOME_LINK, language), 'home')}
              {showProjectBreadcrumb()}
            </div>
          </div>
          <div className='header-right'>
            <div className='navigation search'>
              {this.createNavigationItem('/search', searchLinkContent, 'search')}
            </div>
            <div className='user-name'>
              {getLoggedInUser().fullname}
            </div>
            <button className='logout' onClick={this.logoutButtonWasClicked}>
              {translate(HEADER_LOGOUT_LINK, this.props.language)}
            </button>
          </div>
        </div>
      </header>
    )
  }
}
