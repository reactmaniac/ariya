import React from 'react'
import {setLoggedInUser, setToken} from '../auth/loginUser'
import {translate} from '../localization/localizer'
import {
  ERROR_MESSAGE_LOGIN,
  LOGIN_USERNAME_LABEL,
  LOGIN_PASSWORD_LABEL,
  SIGNIN_BUTTON_TEXT,
} from '../localization/vocabConstants'
import {authenticateUser} from '../fetch/resourceFetcher'

export default class LoginPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      language: 'english'
    }

    this.handleSubmitEvent = this.handleSubmitEvent.bind(this)
  }

  redirectToNextScreen() {
    if (this.props.location && this.props.location.pathname !== '/login') {
      this.props.history.replace(this.props.location.pathname)
      return
    }
    this.props.history.replace('/')
  }

  handleSubmitEvent(event) {
    event.preventDefault()

    if (this.refs) {
      let username = this.refs.usernameInput.value
      let password = this.refs.passwordInput.value

      authenticateUser(username, password)
        .then((response) => {
          if (response.error) {
            this.setState({displayErrorMessageFlag:true})
            return
          }

          setLoggedInUser(response.loggedInUser)
          setToken(response.token)
          this.redirectToNextScreen()
        })
    }
  }

  showErrorMessage() {
    if (this.state.displayErrorMessageFlag) {
      return (
        <div className='error-message'>{translate(ERROR_MESSAGE_LOGIN, this.state.language)}</div>
      )
    }
  }

  render() {
    return (
      <form onSubmit={this.handleSubmitEvent} id='login-form'>
        <div className='username'>
          <label>{translate(LOGIN_USERNAME_LABEL, this.state.language)}
            <input ref='usernameInput' id='username-input'/>
          </label>
        </div>
        <div className='password'>
          <label>{translate(LOGIN_PASSWORD_LABEL, this.state.language)}
            <input ref='passwordInput' type='password' id='password-input'/>
          </label>
        </div>
        {this.showErrorMessage()}
        <button className='sign-in small' type='submit'>
          {translate(SIGNIN_BUTTON_TEXT, this.state.language)}
        </button>
      </form>
    )
  }
}
