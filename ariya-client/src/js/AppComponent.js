import React from 'react'
import {Route} from 'react-router'
import LoginPage from './Login/LoginPage'
import {scrollPageToTop} from './wrappers/windowWrapper'
import AppContainer from './AppContainer'
import {HashRouter, Switch} from 'react-router-dom'
import {isLoggedIn} from './auth/loginUser'

const requireAuth = (props) => {
  if (isLoggedIn()) {
    return (<AppContainer {...props}/>)
  } else {
    return (<LoginPage {...props}/>)
  }
}

export default function AppComponent() {
  return (
      <HashRouter>
        <Switch>
          <Route exact path='/login' component={LoginPage}/>
          <Route path=''
                 render={(props) => {return requireAuth(props)}}
                 onChange={scrollPageToTop}
          />
        </Switch>
      </HashRouter>
  )
}
