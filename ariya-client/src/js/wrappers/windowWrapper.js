export const scrollPageToTop = (prevState, nextState) => {
  if (nextState.location.action !== 'POP') {
    window.scrollTo(0, 0);
  }
}

export const reloadPage = () => {
  window.location.reload()
}

export const scrollToElement = (elementId) => {
  let root = document.getElementById('scroll-root')
  let element = document.getElementById(elementId)
  let y = element.offsetTop - root.offsetTop
  root.scrollTop = y
}

export const currentUrl = () => {
  return window.location.href
}
