import PropTypes from 'prop-types'
import React from 'react'
import {Link} from 'react-router-dom'

export class LinkWrapper extends React.Component {
  render() {
    return (
      <Link className={this.props.className} to={this.props.to}>
        {this.props.children}
      </Link>
    )
  }
}

LinkWrapper.propTypes = {
  className: PropTypes.string,
  to: PropTypes.any.isRequired
}