package biz.infox.exception;

public class PreConditionRequestException extends RuntimeException {

    public PreConditionRequestException(String message){
        super(message);
    }

    public PreConditionRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
