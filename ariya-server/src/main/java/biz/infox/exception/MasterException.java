package biz.infox.exception;


public class MasterException extends RuntimeException {

    public MasterException(String errorMessage) {
        super(errorMessage);
    }
    public MasterException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
