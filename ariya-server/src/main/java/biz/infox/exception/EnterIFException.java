package biz.infox.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class EnterIFException extends RuntimeException {
    public static final int DATA_ERROR = 0;
    public static final int DATA_GET_ERROR = 1;
    public static final int OTHER_ERROR = 2;

    private String path;
    private int errorType;

    public EnterIFException(String errorMessage,String path,int errorType) {
        super(errorMessage);
        this.path = path;
        this.errorType = errorType;
    }

    public EnterIFException(String errorMessage) {
        super(errorMessage);
    }
}
