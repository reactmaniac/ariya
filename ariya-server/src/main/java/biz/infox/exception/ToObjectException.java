package biz.infox.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ToObjectException extends RuntimeException {
    public static final int DATA_ERROR = 0;
    public static final int DATA_GET_ERROR = 1;
    public static final int OTHER_ERROR = 2;

    private String path;
    private int errorType;

    public ToObjectException(String errorMessage, int errorType) {
        super(errorMessage);
        this.errorType = errorType;
    }

    public ToObjectException(String errorMessage) {
        super(errorMessage);
    }
}
