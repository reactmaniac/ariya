package biz.infox.service;

import biz.infox.dataMapper.DriveUserDataMapper;
import biz.infox.dto.DriveUser;
import biz.infox.dto.LoggedInUser;
import biz.infox.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;

import static java.util.Arrays.asList;

@Component
@Transactional
@RequiredArgsConstructor
@Slf4j
public class AuthenticationService {
    private final DriveUserDataMapper driveUserDataMapper;

    public LoggedInUser getLoggedInUser(String userId, String password) {
        List<DriveUser> userRoles;
        try {
            userRoles = driveUserDataMapper.getUserRoles(userId);
        } catch (NotFoundException e) {
            log.warn(e.getMessage());
            userRoles = asList(DriveUser.builder()
                            .id(1L)
                            .languageCode("EN")
                            .dateFormat("yyyy/MM/dd")
                            .timezone("")
                            .localLanguage("EN")
                            .startupFunction("DRIVE")
                            .gdCountryCode("")
                            .gdCompanyCode("")
                            .gdCompanyName("")
                            .gdDepartmentCode("")
                            .gdDepartmentName("")
                            .regionCode("EU")
                            .hubTcs("NE")
                            .regionalTcs("NE")
                            .companyCode("")
                            .availableFunctions(asList("CARS"))
                            .departmentCode("")
                            .carsRoleName("No Role")
                            .displayName("")
                            .countryCode("")
                            .treadCode(null)
                            .fsss(null)
                        .build());
//            throw new UsernameNotFoundException(e.getMessage(), e);
        }

        LoggedInUser loggedInUser = new LoggedInUser(userId, password);
        loggedInUser.setId(userRoles.get(0).getId());
        loggedInUser.setUserId(userRoles.get(0).getUserId());
        loggedInUser.setUserName(userRoles.get(0).getUserName());
        loggedInUser.setMail(userRoles.get(0).getMail());
        loggedInUser.setLanguageCode(userRoles.get(0).getLanguageCode());
        loggedInUser.setDateFormat(userRoles.get(0).getDateFormat());
        loggedInUser.setTimezone(userRoles.get(0).getTimezone());
        loggedInUser.setStartupFunction(userRoles.get(0).getStartupFunction());
        loggedInUser.setLocalLanguage(userRoles.get(0).getLocalLanguage());
        loggedInUser.setGdCountryCode(userRoles.get(0).getGdCountryCode());
        loggedInUser.setGdCompanyCode(userRoles.get(0).getGdCompanyCode());
        loggedInUser.setGdCompanyName(userRoles.get(0).getGdCompanyName());
        loggedInUser.setGdDepartmentCode(userRoles.get(0).getGdDepartmentCode());
        loggedInUser.setGdDepartmentName(userRoles.get(0).getGdDepartmentName());
        loggedInUser.setUserRoles(userRoles);
        loggedInUser.setAuthorities(new HashSet<>());
        if (userRoles.size() == 1) {
            loggedInUser.setUserRoleId(userRoles.get(0).getUserRoleId());
            loggedInUser.setRegionCode(userRoles.get(0).getRegionCode());
            loggedInUser.setHubTcs(userRoles.get(0).getHubTcs());
            loggedInUser.setRegionalTcs(userRoles.get(0).getRegionalTcs());
            loggedInUser.setIncidentCountry(userRoles.get(0).getIncidentCountry());
            loggedInUser.setCompanyCode(userRoles.get(0).getCompanyCode());
            loggedInUser.setAvailableFunctions(userRoles.get(0).getAvailableFunctions());
            loggedInUser.setDepartmentCode(userRoles.get(0).getDepartmentCode());
            loggedInUser.setCarsRoleCode(userRoles.get(0).getCarsRoleCode());
            loggedInUser.setCarsRoleName(userRoles.get(0).getCarsRoleName());
            loggedInUser.setDisplayName(userRoles.get(0).getDisplayName());
            loggedInUser.setCountryCode(userRoles.get(0).getCountryCode());
            loggedInUser.setSangenDisplayName(userRoles.get(0).getSangenDisplayName());
            loggedInUser.setAuthorities(new HashSet<>((AuthorityUtils.createAuthorityList("ROLE_" + loggedInUser.getCarsRoleCode() + "_" + ("Yes".equals(loggedInUser.getCarsApprover())?"APPROVER":"")))));
            loggedInUser.setTreadCode(userRoles.get(0).getTreadCode());
            loggedInUser.setFsss(userRoles.get(0).getFsss());

        }

        return loggedInUser;
    }


}
