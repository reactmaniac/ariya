package biz.infox.repository;

import biz.infox.dataMapper.CompanyDataMapper;
import biz.infox.dto.Company;
import biz.infox.entity.DatabaseCompany;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Repository
public class CompanyRepository {
    private final CompanyDataMapper companyDataMapper;

    public CompanyRepository(CompanyDataMapper companyDataMapper) {
        this.companyDataMapper = companyDataMapper;
    }

    public Company get(String companyCode) {
        Company databaseCompany = companyDataMapper.get(companyCode);
        return new Company(databaseCompany);
    }
}
