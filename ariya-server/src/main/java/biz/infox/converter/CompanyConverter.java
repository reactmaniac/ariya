package biz.infox.converter;

import biz.infox.dto.Company;
import biz.infox.entity.DatabaseCompany;
import biz.infox.sqlUtil.SqlUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
public class CompanyConverter {

    public List<Company> forCompanyList(List<DatabaseCompany> databaseCompanys) {
        List<Company> companys = databaseCompanys.stream().map(databaseCompany -> {

            Company company = forCompany(databaseCompany);

            return company;
        }).collect(toList());

        return companys;
    }

    public Company forCompany(DatabaseCompany databaseCompany) {
        Company company = new Company();

        company.setId(databaseCompany.getId());
        company.setCompanyCode(databaseCompany.getCompanyCode());
        company.setCompanyName(databaseCompany.getCompanyName());
//        company.setRegionalTcs(databaseCompany.getRegionalTcs());
//        company.setCarsRoleCode(SqlUtil.getStringList(databaseCompany.getCarsRoleCode()));
//        company.setCarsReferenceRange(databaseCompany.getCarsReferenceRange());
//        company.setCarsRestrictedAccess(databaseCompany.getCarsRestrictedAccess());
//        company.setCarsTsm(databaseCompany.getCarsTsm());
//        company.setCarsTechPubs(databaseCompany.getCarsTechPubs());
//        company.setSangenRoleCode(SqlUtil.getStringList(databaseCompany.getSangenRoleCode()));
//        company.setIsFirstnameLastname(databaseCompany.getIsFirstnameLastname());

        return company;
    }




}
