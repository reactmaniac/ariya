package biz.infox.controller;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/pets")
public class UserController {

    @GetMapping
    public List<Map<String, String>> pets() {
        return new ArrayList<>();
    }

    @GetMapping("{id}")
    public Map<String, String> pet(@PathVariable String id) {
        return new HashMap<>();
    }

    @PutMapping("{id}")
    public void updatePet(@PathVariable String id) {
        return;
    }

    @PostMapping
    public int insertPet() {
        return 1;
    }

    @DeleteMapping
    public void deletePet() {
        return;
    }
}