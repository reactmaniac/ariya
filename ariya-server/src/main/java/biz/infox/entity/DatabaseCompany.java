package biz.infox.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DatabaseCompany {
    private long id;
    private String companyCode;
    private String companyName;
    private String regionCode;
    private String referenceRange;
}
