package biz.infox.dataMapper;

import biz.infox.converter.CompanyConverter;
import biz.infox.dto.Company;
import biz.infox.entity.DatabaseCompany;
import biz.infox.exception.NotFoundException;
import com.miragesql.miragesql.SqlManager;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static biz.infox.sqlUtil.SqlUtil.sql;

@Repository
public class CompanyDataMapper {
    private final SqlManager sqlManager;
    private final CompanyConverter companyConverter;

    public CompanyDataMapper(
            SqlManager sqlManager,
            CompanyConverter companyConverter) {
        this.sqlManager = sqlManager;
        this.companyConverter = companyConverter;
    }

    public List<DatabaseCompany> getAll(String region) {
        return null;
    }

    public Company get(String companyCode) {
        Map<String, Object> param = new HashMap<>();
        param.put("company_code", companyCode);

        Optional<DatabaseCompany> maybeCompany = Optional.ofNullable(
                sqlManager.getSingleResult(DatabaseCompany.class, sql("sql/company_get.sql"), param)
        );

        DatabaseCompany databaseCompany = maybeCompany.orElseThrow(() -> new NotFoundException("Company is not found." + companyCode));
        return companyConverter.forCompany(databaseCompany);
    }
}
