package biz.infox.config.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class WebServiceAuthenticationProvider implements AuthenticationProvider {
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String inputName = authentication.getName();
        String inputPass = authentication.getCredentials().toString();

        String name = "john";
        String pass = "12345678";

        if (inputName.equals(name) && inputPass.equals(pass)) {
            return new UsernamePasswordAuthenticationToken(inputName, inputPass, authentication.getAuthorities());
        } else {
            throw new BadCredentialsException("ユーザー名やパスワードが正しくない");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

}
