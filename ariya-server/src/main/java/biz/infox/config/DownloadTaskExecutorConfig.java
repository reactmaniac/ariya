package biz.infox.config;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class DownloadTaskExecutorConfig {

    private static final String DOWNLOAD_TASK_MAX_POOL_SIZE = System.getenv("DOWNLOAD_TASK_MAX_POOL_SIZE");

    @Bean
    @Qualifier("downloadTaskExecutor")
    public TaskExecutor downloadTaskExecutor() {
        int maxPoolSize = 6;
        if (DOWNLOAD_TASK_MAX_POOL_SIZE != null) {
            maxPoolSize = Integer.parseInt(DOWNLOAD_TASK_MAX_POOL_SIZE);
        }
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(maxPoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setThreadNamePrefix("download");
        return executor;
    }
}