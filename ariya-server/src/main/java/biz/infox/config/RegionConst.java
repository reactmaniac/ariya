package biz.infox.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RegionConst {
    public static final List<String> chgFieldsHistoryNNA = new ArrayList<>(Arrays.asList("AO","CN","AMI","NA","LA","RN"));
    public static final String chgFieldHistoryNML = "JP";
    public static final String chgFieldHistoryNE = "EU";
    public static final String reqUseLocalRegion = "JP";
    public static final List<String> trDownloadFileNNAandNE = new ArrayList<>(Arrays.asList("AO","CN","AMI","NA","LA","EU","RN"));
}
