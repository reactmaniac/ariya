package biz.infox.sqlUtil;

import com.miragesql.miragesql.ClasspathSqlResource;
import com.miragesql.miragesql.SqlManager;
import com.miragesql.miragesql.SqlResource;

import java.util.*;

public class SqlUtil {
    public static SqlResource sql(String sqlName) {
        return new ClasspathSqlResource(sqlName);
    }

    public static String arrayToString(String[] array) {
        return String.join("\\t", Arrays.asList(array));
    }

    public static String[] stringToArray(String string) {
        if (string == null) {
            return null;
        }
        return string.split("\\\\t");
    }

    public static List<String> getStringList(String values) {
        String[] valueArray = stringToArray(values);

        List<String> stringList = new ArrayList<>();
        if (valueArray == null) {
            return stringList;
        }
        for (int i = 0; i < valueArray.length; i++) {
            stringList.add(valueArray[i]);
        }

        return stringList;
    }



    public static void deleteData(SqlManager sqlManager,Long id, String docType) {
        Map<String, Object> param = new HashMap<>();
        param.put("status", "DELETED");
        param.put("docType", docType);
        param.put("docId", id);
        param.put("table", getWorkflowTableName(docType));

        sqlManager.executeUpdate(sql("sql/logic_delete_data.sql"),param);
    }

    public static String getWorkflowTableName(String documentType) {
        switch (documentType){
            case "ATTACHMENT":
            case "COMMENT":
                return "workflow_cmt_attach";
            case "ADDRESS":
            case "CARS_ROLE":
            case "COMPANY":
            case "DEPARTMENT":
            case "DRIVE_USER":
            case "HUB_TCS":
            case "INFORMATION":
            case "KEYWORD":
            case "PFP":
            case "REGION":
            case "REGIONAL_TCS":
            case "SANGEN_ROLE":
            case "SUPPLIER":
            case "TR_ROUTING":
            case "UMR":
            case "URR":
            case "USER_ROLE":
                return "workflow_common";
            default:
                return "workflow";
        }
    }
}