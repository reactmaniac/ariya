package biz.infox.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class LoggedInUser extends User {
    private long id;
    private String userId;
    private String userName;
    private String mail;
    private String languageCode;
    private String dateFormat;
    private String timezone;
    private String localLanguage;
    private String startupFunction;
    private String gdCountryCode;
    private String gdCompanyCode;
    private String gdCompanyName;
    private String gdDepartmentCode;
    private String gdDepartmentName;
    private Long userRoleId;
    private String regionCode;
    private String hubTcs;
    private String regionalTcs;
    private String incidentCountry;
    private String companyCode;
    private List<String> availableFunctions;
    private String departmentCode;
    private String carsRoleCode;
    private String carsRoleName;
    private String displayName;
    private String sangenDisplayName;
    private String countryCode;
    private String carsFirstContact;
    private String carsApprover;
    private String carsPCNApprover;
    private String carsUserApprover;
    private String carsReadOnly;
    private String carsRestrictedAccess;
    private String carsTsm;
    private String carsTechPubs;
    private String sangenRoleCode;
    private String sangenFirstContact;
    private String sangenBreakdownEngineer;
    private List<String> sangenTerritory;
    private List<String> sangenModel;
    private String sangenResponsibility;
    private String referenceRange;
    private String referenceRangeCompany;
    private String referenceRangeDepartment;
    private List<DriveUser> userRoles;
    private String samlLoginId;
    private Long sangenRoleId;
    private String treadCode;
    private String fsss;
    private String surName;
    private String givenName;
    private String remoteIDP;

    public LoggedInUser(String userId, String password) {
        super(userId, password, new ArrayList<>());
        this.userId = userId;
        this.password = password;
        this.username = userId;
        this.enabled = true;
        this.accountNonExpired = true;
        this.accountNonLocked = true;
        this.credentialsNonExpired = true;
    }

    @JsonIgnore
    public DriveUser getDriveUser() {
        DriveUser driveUser = new DriveUser();
        driveUser.setId(this.id);
        driveUser.setUserId(this.userId);
        driveUser.setUserName(this.userName);
        driveUser.setMail(this.mail);
        driveUser.setGdCompanyCode(this.gdCompanyCode);
        driveUser.setGdDepartmentCode(this.gdDepartmentCode);
        driveUser.setUserRoleId(this.userRoleId);
        driveUser.setRegionCode(this.regionCode);
        driveUser.setCompanyCode(this.companyCode);
        driveUser.setDepartmentCode(this.departmentCode);
        driveUser.setCarsRoleCode(this.carsRoleCode);
        driveUser.setCarsRoleName(this.carsRoleName);
        driveUser.setDisplayName(this.displayName);
        driveUser.setSangenDisplayName(this.sangenDisplayName);
        driveUser.setGdCountryCode(this.gdCountryCode);
        driveUser.setGdCompanyName(this.gdCompanyName);
        driveUser.setGdDepartmentName(this.gdDepartmentName);
        driveUser.setLanguageCode(this.languageCode);
        driveUser.setDateFormat(this.dateFormat);
        driveUser.setStartupFunction(this.startupFunction);
        driveUser.setTimezone(this.timezone);
        driveUser.setCountryCode(this.countryCode);
        driveUser.setTreadCode(this.treadCode);
        driveUser.setFsss(this.fsss);
        driveUser.setSurName(this.surName);
        driveUser.setGivenName(this.givenName);

        return driveUser;
    }

    public String getUserName() { return userName; }

    private String username;
    @JsonIgnore
    @Override
    public String getUsername() {
        return username;
    }

    private Set<GrantedAuthority> authorities;
    @JsonIgnore
    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    private String password;
    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    private boolean enabled;
    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    private boolean accountNonExpired;
    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    private boolean accountNonLocked;
    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    private boolean credentialsNonExpired;
    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }


}
