package biz.infox.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DriveUser implements Serializable {
    private static final long serialVersionUID = 1L;
    private long id;
    private String userId;
    private String userName;
    private String mail;
    private String phone;
    private String languageCode;
    private String dateFormat;
    private String timezone;
    private String localLanguage;
    private String startupFunction;
    private String gdCountryCode;
    private String gdCompanyCode;
    private String gdCompanyName;
    private String gdDepartmentCode;
    private String gdDepartmentName;
    private String regionCode;
    private String hubTcs;
    private String regionalTcs;
    private String incidentCountry;
    private String companyCode;
    private List<String> availableFunctions;
    private String carsReferenceRangeCompany;
    private String departmentCode;
    private String carsReferenceRangeDepartment;
    private String carsRoleName;
    private String sangenRoleName;
    private Long userRoleId;
    private String carsRoleCode;
    private String countryCode;

    private Long roleIndex;
    private String displayName;
    private String sangenDisplayName;
    private String treadCode;
    private String fsss;
    private String surName;
    private String givenName;


    public boolean hasUserInformation() {
        return this.userId != null && this.carsRoleCode != null && this.companyCode != null && this.departmentCode != null;
    }



    public boolean isSameDept(DriveUser otherUser) {
        return otherUser != null
                && this.companyCode != null
                && this.departmentCode != null
                && this.companyCode.equals(otherUser.getCompanyCode())
                && this.departmentCode.equals(otherUser.getDepartmentCode())
                ;
    }

    public boolean isEqualUser(DriveUser otherUser) {
        return otherUser != null
                && (this.companyCode != null && this.companyCode.equals(otherUser.getCompanyCode()))
                && (this.departmentCode != null && this.departmentCode.equals(otherUser.getDepartmentCode()))
                && (this.userId != null && this.userId.equals(otherUser.getUserId()))
                && (this.carsRoleCode!= null && this.carsRoleCode.equals(otherUser.getCarsRoleCode()));
    }

}
