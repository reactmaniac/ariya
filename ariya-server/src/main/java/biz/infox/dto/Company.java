package biz.infox.dto;

import biz.infox.entity.DatabaseCompany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Company {
    private long id;
    private String companyCode;
    private String companyName;
    private String regionCode;
    private String referenceRange;

    public Company(Company databaseCompany) {
        this.id = databaseCompany.getId();
        this.companyCode = databaseCompany.getCompanyCode();
        this.companyName = databaseCompany.getCompanyName();
        this.regionCode = databaseCompany.getRegionCode();
        this.referenceRange = databaseCompany.getReferenceRange();
    }
}
