-- Project Name : noname
-- Date/Time    : 2020/02/26 9:19:38
-- Author       : FJT02673
-- RDBMS Type   : PostgreSQL
-- Application  : A5:SQL Mk-2

drop table if exists address cascade;
create table address (
    id bigserial not null
  , address_type text not null
  , company_name text not null
  , pickup_territory text
  , nickname text
  , contact_person text
  , phone_number text
  , country_code text
  , street_number text
  , city text
  , district text
  , postal_code text
  , created_on timestamp(6) without time zone
  , modified_on timestamp(6) without time zone
  , parent_address_id bigint
  , regional_tcs text[]
) ;

create unique index address_pki
  on address(id);

alter table address
  add constraint address_pkc primary key (id);



drop table if exists user_role cascade;

create table user_role (
    id bigserial not null
  , company_code text not null
  , department_code text not null
  , user_id text not null
  , country_code text not null
  , cars_role_code text default 'NO_ROLE' not null
  , cars_reference_range text
  , cars_first_contact text
  , cars_approver text
  , cars_pcn_approver text
  , cars_user_approver text
  , cars_read_only text
  , cars_restricted_access text
  , cars_tsm text
  , cars_tech_pubs text
  , sangen_role_code text default 'NO_ROLE' not null
  , sangen_first_contact text
  , sangen_breakdown_engineer text
  , sangen_territory text[]
  , sangen_model text[]
  , sangen_responsibility text
  , last_login_date timestamp(6) without time zone
  , created_on timestamp(6) without time zone
  , modified_on timestamp(6) without time zone
) ;

create unique index user_role_ix1
  on user_role(company_code,department_code,user_id,cars_role_code,sangen_role_code);

create unique index user_role_pki
  on user_role(id);

alter table user_role
  add constraint user_role_pkc primary key (id);

drop table if exists department cascade;

create table department (
    id bigserial not null
  , company_code text not null
  , department_code text not null
  , department_name text not null
  , cars_role_code text[]
  , cars_reference_range text
  , cars_restricted_access text
  , cars_tsm text
  , cars_tech_pubs text
  , sangen_role_code text[]
  , created_on timestamp(6) without time zone
  , modified_on timestamp(6) without time zone
) ;

create unique index department_ix1
  on department(company_code,department_code);

create unique index department_pki
  on department(id);

alter table department
  add constraint department_pkc primary key (id);

drop table if exists company cascade;

create table company (
    id bigserial not null
  , company_code text not null
  , company_name text not null
  , regional_tcs text not null
  , available_functions text[] not null
  , language_code text not null
  , date_format text not null
  , timezone text not null
  , local_language text
  , startup_function text
  , cars_role_code text[]
  , cars_reference_range text
  , cars_restricted_access text
  , cars_tsm text
  , cars_tech_pubs text
  , sangen_role_code text[]
  , created_on timestamp(6) without time zone
  , modified_on timestamp(6) without time zone
  , is_firstname_lastname boolean default true not null
) ;

create unique index company_ix1
  on company(company_code);

create unique index company_pki
  on company(id);

alter table company
  add constraint company_pkc primary key (id);

drop table if exists region cascade;



comment on column department.id is 'ID';
comment on column department.company_code is '企業コード';
comment on column department.department_code is '部署コード';
comment on column department.department_name is '部署名';
comment on column department.cars_role_code is 'CARS ロールコード';
comment on column department.cars_reference_range is 'CARS 参照可能範囲';
comment on column department.cars_restricted_access is 'CARS アクセス制限';
comment on column department.cars_tsm is 'CARS TSM';
comment on column department.cars_tech_pubs is 'CARS Tech Pubs';
comment on column department.sangen_role_code is 'SANGEN ロールコード';
comment on column department.created_on is '作成日';
comment on column department.modified_on is '更新日';

comment on column company.id is 'ID';
comment on column company.company_code is '企業コード';
comment on column company.company_name is '企業名';
comment on column company.regional_tcs is 'Regional TCS';
comment on column company.available_functions is '利用可能機能';
comment on column company.language_code is '言語コード';
comment on column company.date_format is '日付形式';
comment on column company.timezone is 'タイムゾーン';
comment on column company.local_language is 'Local Language項目表示有無';
comment on column company.startup_function is 'Startup Function';
comment on column company.cars_role_code is 'CARS ロールコード';
comment on column company.cars_reference_range is 'CARS 参照可能範囲';
comment on column company.cars_restricted_access is 'CARS アクセス制限';
comment on column company.cars_tsm is 'CARS TSM';
comment on column company.cars_tech_pubs is 'CARS Tech Pubs';
comment on column company.sangen_role_code is 'SANGEN ロールコード';
comment on column company.created_on is '作成日';
comment on column company.modified_on is '更新日';
comment on column company.is_firstname_lastname is 'ユーザ名表示FirstNameLastName';


