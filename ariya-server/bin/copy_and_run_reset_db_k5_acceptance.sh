#!/bin/sh -x

scp -i /Users/fqidrive/workspace/VPN/sshkey/COMMON-001_KP_01.pem -r ./bin/reset_db_k5_acceptance.sh k5user@10.0.0.200:~/acceptance_dir
scp -i /Users/fqidrive/workspace/VPN/sshkey/COMMON-001_KP_01.pem -r ./src/main/resources/db/* k5user@10.0.0.200:~/acceptance_dir
ssh -i /Users/fqidrive/workspace/VPN/sshkey/COMMON-001_KP_01.pem k5user@10.0.0.200 env PGHOST=$PGHOST PGPORT=$PGPORT PGDATABASE=$PGDATABASE PGUSER=$PGUSER PGPASSWORD=$PGPASSWORD sh acceptance_dir/reset_db_k5_acceptance.sh
