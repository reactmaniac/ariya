#!/bin/sh
./bin/make_json.sh

export CLIENT_URL=http://localhost:8000
export DATABASE_URL=jdbc:postgresql://localhost/fqidrive_test
export DATABASE_USERNAME=pivotal
export DATABASE_PASSWORD=
export SP_METADATA_URL=https://fqidrive-localhost.jp-east-2.cf-app.net/saml/metadata
export IDP_METADATA_URL=https://gdauth.qa.jp.nissan.biz/nidp/saml2/metadata

./bin/db_resetter.sh fqidrive_test
./bin/db_resetter.sh fqidrive_test2
./bin/db_resetter.sh fqidrive_test3
./bin/db_resetter.sh fqidrive_test4
./bin/db_resetter.sh fqidrive_test5

./gradlew clean build test --stacktrace
./gradlew clean build integrationTest -x test --stacktrace
