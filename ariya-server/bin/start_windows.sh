#!/bin/sh

export IP_ADDRESS=`ipconfig getifaddr en0`
export CLIENT_URL=http://$IP_ADDRESS:8000

./gradlew clean build

java -jar build/libs/eva-0.0.1-SNAPSHOT.jar